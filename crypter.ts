import { Xorer } from './xorer'
import { Plain } from './plain'
import { Readable, Writable, Transform } from 'stream'
import { ReadableStreamBuffer, WritableStreamBuffer } from 'stream-buffers'
import { createGzip, createGunzip } from 'zlib'
import {
  createHash,
  scryptSync,
  createCipheriv,
  createDecipheriv,
} from 'crypto'
import { pipeline } from 'stream'
const MAGIC = 'SC01'
export enum Modes { "None", "Plain", "Xored" }

export class Crypter {
  private algo = "aes-192-cbc"
  private key: any
  private iv: Buffer = Buffer.alloc(16, 0)
  private xin: Buffer = Buffer.alloc(10)

  constructor(passphrase: string, private salt: string = "someSalt") {
    this.setPassword(passphrase)
  }
  public setPassword(passphrase: string): void {
    // console.log("Store: Setting password " + passphrase)
    const hash = createHash('sha256')
    const pwd = hash.update(passphrase); // .update(this.salt)
    this.key = scryptSync(passphrase, this.salt, 24)

    const ivraw = pwd.digest()
    this.iv = Buffer.from(ivraw.subarray(0, 16))
    this.xin = Buffer.from(ivraw.subarray(Math.min(200, passphrase.length), 5))
  }

  public createStreams(source: Readable | Buffer, dest: Writable | Buffer) {
    let instream, outstream
    if (source instanceof Buffer) {
      instream = new ReadableStreamBuffer()
        ; (instream as ReadableStreamBuffer).put(source)
        ; (instream as ReadableStreamBuffer).stop()
    } else {
      instream = source as Readable
    }
    if (dest instanceof Buffer) {
      outstream = new WritableStreamBuffer()
    } else {
      outstream = dest as Writable
    }
    return { instream, outstream }
  }

  public async encryptBuffer(source: Buffer, mode: Modes = Modes.Xored): Promise<Buffer> {
    let instream, outstream
    instream = new ReadableStreamBuffer()
    instream.put(source)
    instream.stop()
    outstream = new WritableStreamBuffer()
    await this.encrypt(instream, outstream, mode)
    const result = outstream.getContents() as Buffer
    return result
  }

  public async decryptBuffer(source: Buffer): Promise<Buffer> {
    let instream, outstream
    instream = new ReadableStreamBuffer()
    instream.put(source)
    instream.stop()
    outstream = new WritableStreamBuffer()
    await this.decrypt(instream, outstream)
    const result = outstream.getContents() as Buffer
    return result
  }

  public async encryptStreamToBuffer(instream:Readable, mode:Modes=Modes.Xored): Promise<Buffer>{
    const outstream=new WritableStreamBuffer()
    await this.encrypt(instream,outstream,mode)
    const result=outstream.getContents() as Buffer
    return result;
  }

  public async decryptStreamToBuffer(instream:Readable) : Promise<Buffer>{
    const outstream=new WritableStreamBuffer()
    await this.decrypt(instream,outstream)
    const result=outstream.getContents() as Buffer
    return result;
  }

  public encrypt(instream: Readable, outstream: Writable, mode: Modes = Modes.Xored): Promise<void> {
    return new Promise((resolve, reject) => {
      const cipher = createCipheriv(this.algo, this.key, this.iv)
      const gz = createGzip()
      let transformer: Transform
      switch (mode) {
        case Modes.Xored:
          transformer = new Xorer(this.xin)
          break;
        default:
          transformer = new Plain(); break;

      }
      if (mode == Modes.None) {
        pipeline(instream, outstream, err => {
          if (err) {
            reject("pipeline direct " + err)
          } else {
            outstream.end()
            resolve()
          }
        })
      } else {

        if (mode != Modes.Plain) {
          outstream.write(MAGIC)
        }
        try {
          pipeline(instream, gz, transformer, cipher, outstream, (err) => {
            if (err) {
              reject('pipeline ' + err)
            } else {
              outstream.end()
              resolve()
            }
          })
        } catch (err) {
          console.log(err)
          reject(err)
        }
      }
    })

  }

  public decrypt(instream: Readable, outstream: Writable, plain: boolean = false): Promise<void> {
    return new Promise((resolve, reject) => {
      if (plain) {
        pipeline(instream, outstream, err => {
          if (err) {
            reject(err)
          } else {
            outstream.end()
            resolve()
          }
        })
      } else {
        const decipher = createDecipheriv(this.algo, this.key, this.iv)
        const gz = createGunzip()
        let transformer: Transform
        const readHeader = () => {
          const header = instream.read(MAGIC.length)
          if (header.toString() == MAGIC) {
            transformer = new Xorer(this.xin)
          } else {
            transformer = new Plain()
            decipher.write(header)
          }
          instream.off('readable', readHeader)
          try {
            pipeline(instream, decipher, transformer, gz, outstream, (err) => {
              if (err) {
                reject('pipeline ' + err)
              } else {
                outstream.end()
                resolve()
              }
            })
          } catch (err) {
            console.log(err)
            reject(err)
          }

        }
        instream.on('readable', readHeader)
      }
    })
  }
}
