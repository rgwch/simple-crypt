import { Transform } from 'stream'

export class Xorer extends Transform {
    oper:Uint8Array;
  constructor(private x: Uint8Array|string) {
    super()
    if(typeof(x)=='string'){
        this.oper=Buffer.from(x)
    }else{
        this.oper=x
    }
  }
  public _flush(callback: (error?: Error) => void) {
    callback()
  }
  public _transform(
    chunk: Buffer,
    encoding: string,
    callback: (err?: Error, data?: Buffer) => void,
  ) {
      let i=0
    const ex=chunk.map((b) => b ^ this.oper[i++%this.oper.length])
    callback(undefined, Buffer.from(ex))
  }
}
