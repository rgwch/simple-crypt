import { randomBytes } from 'crypto'
import { Crypter, Modes } from './crypter'
import { ReadableStreamBuffer, WritableStreamBuffer } from 'stream-buffers'
import { pipeline } from 'stream'

describe('crypter', () => {
  let input: Buffer
  let crypter: Crypter
  let output: Buffer
  beforeEach(() => {
    input = randomBytes(2345)
    crypter = new Crypter('some Passphrase', 'holladiho')
    output = Buffer.alloc(input.length)
  })
  it("encrypts buffers", async ()=>{
    const enc=await crypter.encryptBuffer(input)
    const dec=await crypter.decryptBuffer(enc)
    expect(dec).toEqual(input)
  })
  it('should encrypt and decrypt a buffer without pretransform', async () => {
    const s = crypter.createStreams(input, output)
    await crypter.encrypt(s.instream, s.outstream, Modes.Plain)
    const sout: WritableStreamBuffer = s.outstream as WritableStreamBuffer
    const buf = sout.getContents() as Buffer
    expect(buf).toBeTruthy()
    const s2 = crypter.createStreams(buf, output)
    await crypter.decrypt(s2.instream, s2.outstream)
    const result = (s2.outstream as WritableStreamBuffer).getContents()
    expect(result).toEqual(input)
  })

  it('should encrypt and decrypt a buffer with XOR pretransform', async () => {
    const s = crypter.createStreams(input, output)
    await crypter.encrypt(s.instream, s.outstream, Modes.Xored)
    const sout: WritableStreamBuffer = s.outstream as WritableStreamBuffer
    const s2 = crypter.createStreams(sout.getContents() as Buffer, output)
    await crypter.decrypt(s2.instream, s2.outstream)
    const result = (s2.outstream as WritableStreamBuffer).getContents()
    expect(result).toEqual(input)
  })

  it('should encrypt and decrypt a stream with implicit pretransform', async () => {
    const is = new ReadableStreamBuffer()
    const os = new WritableStreamBuffer({ initialSize: 100 })
    is.put(input)
    is.stop()
    await crypter.encrypt(is, os)
    const encrypted = os.getContents()
    expect(encrypted).toBeTruthy()
    const es = new ReadableStreamBuffer()
    const ds = new WritableStreamBuffer({ initialSize: 100 })
    es.put(encrypted as Buffer)
    es.stop()
    await crypter.decrypt(es, ds)
    const decrypted = ds.getContents()
    expect(decrypted).toBeTruthy()
    expect(Buffer.compare(decrypted as Buffer, input)).toBe(0)
  })
  it('should encrypt and decrypt a stream without pretransform', async () => {
    const is = new ReadableStreamBuffer()
    const os = new WritableStreamBuffer({ initialSize: 100 })
    is.put(input)
    is.stop()
    await crypter.encrypt(is, os, Modes.Plain)
    const encrypted = os.getContents()
    expect(encrypted).toBeTruthy()
    const es = new ReadableStreamBuffer()
    const ds = new WritableStreamBuffer({ initialSize: 100 })
    es.put(encrypted as Buffer)
    es.stop()
    await crypter.decrypt(es, ds)
    const decrypted = ds.getContents()
    expect(decrypted).toBeTruthy()
    expect(Buffer.compare(decrypted as Buffer, input)).toBe(0)
  })
  it('should plaintext transmit a stream', async () => {
    const is = new ReadableStreamBuffer()
    const os = new WritableStreamBuffer({ initialSize: 100 })
    is.put(input)
    is.stop()
    await crypter.encrypt(is, os, Modes.None)
    const encrypted = os.getContents()
    expect(encrypted).toBeTruthy()
    const es = new ReadableStreamBuffer()
    const ds = new WritableStreamBuffer({ initialSize: 100 })
    es.put(encrypted as Buffer)
    es.stop()
    await crypter.decrypt(es, ds, true)
    const decrypted = ds.getContents()
    expect(decrypted).toBeTruthy()
    expect(Buffer.compare(decrypted as Buffer, input)).toBe(0)
  })

  it('should encrypt a stream to a Buffer',async()=>{
    const is=new ReadableStreamBuffer()
    is.put(input)
    is.stop()
    const encrypted= await crypter.encryptStreamToBuffer(is)
    const decrypted=await crypter.decryptBuffer(encrypted)
    expect(decrypted).toBeTruthy()
    expect(Buffer.compare(decrypted as Buffer, input)).toBe(0)
  })  

  it('should decrypt a stream to a Buffer', async ()=>{
    const encrypted=await crypter.encryptBuffer(input)
    const is=new ReadableStreamBuffer()
    is.put(encrypted)
    is.stop()
    const decrypted=await crypter.decryptStreamToBuffer(is)
    expect(decrypted).toBeTruthy()
    expect(Buffer.compare(decrypted as Buffer, input)).toBe(0)
    
  })
})

