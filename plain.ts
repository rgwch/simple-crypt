import { Transform } from 'stream'

export class Plain extends Transform {
  constructor() {
    super() 
  }
  public _flush(callback: (error?: Error) => void) {
    callback()
  }
  public _transform(
    chunk: Buffer,
    encoding: string,
    callback: (err?: Error, data?: Buffer) => void,
  ) {
      let i=0
    callback(undefined, chunk)
  }
}
