import { Transform, Readable, Writable, pipeline } from 'stream'
import { ReadableStreamBuffer, WritableStreamBuffer } from 'stream-buffers'

import * as fs from 'fs'
const testdata =
  'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
const version='01'
describe('streams', () => {
  beforeAll((done) => {
    fs.writeFile('__test__.1', testdata, (err) => {
      done()
    })
  })
  afterAll((done) => {
    for (let f of ['__test__.1', '__test__.2', '__test__.3']) {
      fs.rm(f, (err) => {})
    }
    done()
  })
  it('writes and reads a stream from a buffer', (done) => {
    const instream = new ReadableStreamBuffer()
    instream.put(testdata)
    instream.stop()
    const inter = new WritableStreamBuffer()
    inter.write(version)
    pipeline(instream, inter, async (err) => {
      expect(err).toBeFalsy()
      const in2 = new ReadableStreamBuffer()
      in2.put(inter.getContents() as Buffer)
      in2.stop()
      let header

      const readHeader = () => {
        header = in2.read(2).toString()
        expect(header).toEqual(version)
        in2.off('readable', readHeader)
        pipeline(in2, out2, (err) => {
          expect(err).toBeFalsy()
          const check = out2.getContentsAsString()
          expect(check).toEqual(testdata)
          done()
        })
      }

      in2.on('readable', readHeader)
      const out2 = new WritableStreamBuffer()
    })
  })
  it('writes and reads a stream from a file', (done) => {
    const instream = fs.createReadStream('__test__.1')
    const outstream = fs.createWriteStream('__test__.2')
    outstream.write('+-+')
    pipeline(instream, outstream, async (err) => {
      expect(err).toBeFalsy()
      const in2 = fs.createReadStream('__test__.2')
      let header

      const readHeader = () => {
        header = in2.read(3).toString('utf-8')
        expect(header).toEqual('+-+')
        in2.off('readable', readHeader)
        pipeline(in2, out2, (err) => {
          expect(err).toBeFalsy()
          fs.readFile('__test__.3', (err, check) => {
            expect(err).toBeFalsy()
            expect(check.toString('utf-8')).toEqual(testdata)
            done()
          })
        })
      }

      in2.on('readable', readHeader)
      const out2 = fs.createWriteStream('__test__.3')
    })
  })
})
