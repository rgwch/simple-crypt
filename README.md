# Simple-Crypt.ts

A simple typescript-wrapper around encrypting Node-Buffers and Streams. To make automated brute-force attacks (slightly) more difficult, it adds a 
simple pre-encrypt transform step.

## Installation

`npm i --save @rgwch/simple-crypt`

## Usage

With Streams:

````ts
import {Crypter} from '@rgwch/simple-crypt'
const crypter=new Crypter("super safe passphrase","program specific salt")
const instream=someDataFromSomewhere()
const outstream=sendDateThere()

crypter.encrypt(instream,outstream).then(()=>{
  console.log("success")
}.catch(err=>{
  console.log("Something went wrong: "+err)
})

crypter.decrypt(encryptedStream,outstream).then(()=>{
  console.log("success")
}).catch(err=>{
  console.log("something went wrong: "+err)
})
````

With Buffers:

```ts
import {Crypter} from '@rgwch/simple-crypt'
const crypter=new Crypter("super safe passphrase","program specific salt")
const input=Buffer.from("A Buffer with some data")
const encrypted=await crypter.encryptBuffer(input)

const output=await crypter.decryptBuffer(encrypted)
console.log(output.toString())  // A Buffer with some data


```

Stream to Buffer:

```ts
import {Crypter} from '@rgwch/simple-crypt'
const crypter=new Crypter("super safe passphrase","program specific salt")
const instream=someDataFromSomewhere()
const encrypted=await crypter.encryptStreamToBuffer(instream)

const instreamenc=someEncryptedData()
const decrypted=await crypter.decryptStreamToBuffer(instreamenc)

```


## Test

`npm test`


## License

BSD
